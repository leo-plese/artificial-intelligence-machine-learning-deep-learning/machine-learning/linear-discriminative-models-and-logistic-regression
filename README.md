Linear Discriminative Models and Logistic Regression. Tasks:

1 Linear regression as a classifier

2 Multiclass classification

3 Logistic regression

4 Logistic regression analysis

5 Regularized logistic regression

6 Logistic regression with mapping function

Implemented in Python using NumPy, scikit-learn and Matplotlib library. Code and task description in the Jupyter Notebook.

My lab assignment in Machine Learning, FER, Zagreb.

Created: 2020
